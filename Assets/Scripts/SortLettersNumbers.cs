﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.IO;

public class SortLettersNumbers : MonoBehaviour
{
	public SwipeField SwipeField;
	public CountdownClock Clock;
	public Scores Scores;

	
	public float CorrectAddition;
	public float WrongSubstraction;

	ColorSpace _colorSpace; 
	public List<ColorCard> Cards = new List<ColorCard>();

	void Awake()
	{
		string jsonPath = Path.Combine(Application.streamingAssetsPath, "Colors/real.json");
		string rawPath = Path.Combine(Application.streamingAssetsPath, "Colors/raw.txt");
		
		_colorSpace = new ColorSpace();
		
		if (Application.isEditor)
			_colorSpace.RawToJSONFile(rawPath, jsonPath);
		
		_colorSpace.ColorsFromFile(jsonPath);
	}

	void Start()
	{
		Screen.orientation = ScreenOrientation.Portrait;

		SwipeField.OnUserEndSwipe.AddListener(OnUserSwipe);
		SwipeField.OnCardSwiped.AddListener(OnSwipeEnd);

		SwipeField.Cards.AddRange(Cards.ToArray());
		UpdateCards();
		Scores.Reset();
	}

	void Update()
	{
		if (Input.GetKeyDown(KeyCode.LeftArrow))
			SwipeField.TriggerSwipe(SwipeDirection.Left);
		if (Input.GetKeyDown(KeyCode.RightArrow))
			SwipeField.TriggerSwipe(SwipeDirection.Right);
		if (Input.GetKeyDown(KeyCode.UpArrow))
			SwipeField.TriggerSwipe(SwipeDirection.Up);
	}
	
	void OnUserSwipe(SwipeArea area, SwipeCard card)
	{
		if (area.Direction == SwipeDirection.None)
			SwipeField.SwipeCardBeckToDeck(card);
		else
			SwipeField.SwipeCardAway(area, card);

	}

	void OnSwipeEnd(SwipeArea area, SwipeCard card)
	{
		if (area.Direction == SwipeDirection.None)
			return;
		
		if (area.Direction == SwipeDirection.Up)
		{
			SwipeField.AddCardToDeck(card);
			SwipeField.SwipeCardBeckToDeck(card);
			return;
		}

		bool correct = DirectionAndTypeAreEqual(area.Direction, card.Type);
		Scores.WasCorrect(correct);
		if (correct)
			Clock.AddTime(CorrectAddition);
		else
			Clock.AddTime(-WrongSubstraction);

		card.gameObject.SetActive(false);

		ColorData data = (Random.Range(0, 2) <= 0)?
			_colorSpace.GetFakeRandomColor():
			_colorSpace.GetRealRandomColor();
		
		SetCard(card as ColorCard, data);
		card.gameObject.SetActive(true);
		SwipeField.AddCardToDeck(card);
		SwipeField.SwipeCardBeckToDeck(card);
	}

	bool DirectionAndTypeAreEqual(SwipeDirection dir, int type)
	{
		if (dir == SwipeDirection.Right && type == 1)
			return true;
		if (dir == SwipeDirection.Left && type == 0)
			return true;
		return false;
	}

	void UpdateCards()
	{
		for (int i = 0; i < Cards.Count; i++)
		{
			var newCard = Cards[i];
			ColorData data = (Random.Range(0, 1) <= 0)?
				_colorSpace.GetFakeRandomColor():
				_colorSpace.GetRealRandomColor();
			
			SetCard(newCard, data);
			newCard.gameObject.SetActive(true);
			SwipeField.AddCardToDeck(newCard);
			SwipeField.SwipeCardBeckToDeck(newCard);
		}
	}

	void SetCard(ColorCard card, ColorData data)
	{
		card.SetName(data.Name);
		card.SetDesc("#" + data.Hex);

		Color c;
		ColorUtility.TryParseHtmlString("#" + data.Hex, out c);
		card.SetColor(c);
		card.Type = (data.IsFake)? 0 : 1;
		card.Data = data;
	}
}
