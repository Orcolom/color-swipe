﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct ColorDataFile
{
	public ColorData[] Colors;
}

[System.Serializable]
public struct ColorData
{
	public string Name;
	public string Hex;
	[System.NonSerialized] public bool IsFake;
}
