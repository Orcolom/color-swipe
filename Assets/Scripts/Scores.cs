﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Scores : MonoBehaviour
{
  public Text ScoreLabel;
  public Text CorrectLabel;
  public Text WrongLabel;

  int _correct;
  int _wrong;
  int _score;

  public void Reset()
  {
    _score = 0;
    _wrong = 0;
    _correct = 0;
    UpdateLabels();
  }

  public void WasCorrect(bool b)
  {
    if (b)
      _correct++;
    else
      _wrong++;
    
    _score = _correct - _wrong;
    UpdateLabels();
  }

  void UpdateLabels()
  {
    ScoreLabel.text = _score.ToString();
    CorrectLabel.text = "+ " + _correct.ToString();
    WrongLabel.text = "- " + _wrong.ToString();
  }
}
