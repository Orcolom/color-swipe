﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Globalization;

public class SwipeCard : MonoBehaviour
{
	[Header("Swipe Field")]
	public int Type;
	public object Data;
	public SwipeField SwipeField;
}
