﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwipeArea : MonoBehaviour
{
	public RectTransform AreaTransform;
	public RectTransform SwipeTarget;
	public SwipeDirection Direction;
	public OnCardEvent OnUserEndSwipe;
	public OnCardEvent OnCardSwiped;
}
