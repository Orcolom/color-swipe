﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;

public enum SwipeDirection
{
	None = 0,

	Up = 1,
	Right = 2,
	Down = 4,
	Left = 8,

	UpLeft = Up | Left,
	UpRight = Up | Right,
	DownLeft = Down | Left,
	DownRight = Down | Right,
}

[System.Serializable]
public class OnCardEvent : UnityEvent<SwipeArea, SwipeCard> {}

public class SwipeField : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
	[Header("Cards")]
	public List<SwipeCard> Cards;
	public SwipeCard ActiveCard { get { return Cards.Count > 0? Cards[0] : null; }}

	[Header("Deck")]
	public SwipeArea DeckArea;
	public Transform DeckCardParent;
	Vector2 InitCardPosition { get { return DeckArea.transform.position; }}

	[Header("SwipeAreas")]
	public SwipeArea[] Areas;

	[Header("Events")]
	public OnCardEvent OnUserEndSwipe;
	public OnCardEvent OnCardSwiped;

	Vector2 _dragPosition;
	SwipeDirection _direction;
	Vector2 _cardOffset;

	void Awake()
	{
		SortCards();
	}

	void SortCards()
	{
		var copyCards = new List<SwipeCard>(Cards);
		Cards.Clear();

		foreach (var card in copyCards)
			AddCardToDeck(card);
	}

	public void RemoveCardFromDeck(SwipeCard card)
	{
		Cards.Remove(card);
		card.SwipeField = null;
	}

	public void AddCardToDeck(SwipeCard card)
	{
		if (Cards.Contains(card))
			Cards.Remove(card);
		Cards.Add(card);
		card.transform.SetParent(DeckArea.transform);
		card.transform.SetAsFirstSibling();
		card.SwipeField = this;
	}

	public void ShuffleCards()
	{
		for (int i = 0; i < 50; i++)
		{
			var a = Random.Range(0, Cards.Count);
			var b = Random.Range(0, Cards.Count);

			var card1 = Cards[a];
			Cards[a] = Cards[b];
			Cards[b] = card1;
		}

		foreach (var card in Cards)
			card.transform.SetAsFirstSibling();
	}

	public void SwipeCardBeckToDeck(SwipeCard card)
	{
		StartSwipeCoroutine(card, DeckArea, InitCardPosition);
	}

	public void SwipeCardAway(SwipeArea area, SwipeCard card)
	{
		RemoveCardFromDeck(card);
		StartSwipeCoroutine(card, area, InitCardPosition);
	}

	public void OnBeginDrag(PointerEventData data)
	{
		_cardOffset = InitCardPosition - data.position;
		_direction = SwipeDirection.None;

		CalculateDrag(data);
	}
	
	public void OnDrag(PointerEventData data)
	{
		CalculateDrag(data);

		ActiveCard.transform.position = _dragPosition + _cardOffset;
	}

	public void OnEndDrag(PointerEventData data)
	{
		CalculateDrag(data);

		TriggerSwipe(_direction);
	}

	SwipeArea GetArea(SwipeDirection dir)
	{
		SwipeArea foundArea = null;
		foreach (var area in Areas)
		{
			if (area.Direction == dir)
			{
				foundArea = area;
				break;
			}
		}

		if (foundArea == null)
			return DeckArea;
		return foundArea;
	}

	public void TriggerSwipe(SwipeDirection dir)
	{
		var area = GetArea(dir);
		
		var card = ActiveCard;
		OnUserEndSwipe.Invoke(area, card);
		area.OnUserEndSwipe.Invoke(area, card);
	}

	void CalculateDrag(PointerEventData data)
	{
		if (RectTransformUtility.RectangleContainsScreenPoint(DeckArea.AreaTransform, data.position + _cardOffset))
			_direction = SwipeDirection.None;

		var originPos = DeckArea.transform.position;
		foreach (var area in Areas)
		{
			if (!area.AreaTransform.gameObject.activeInHierarchy)
				continue;
			
			if (RectTransformUtility.RectangleContainsScreenPoint(area.AreaTransform, data.position + _cardOffset))
				_direction = area.Direction;
		}
		_dragPosition = data.position;
	}

	void StartSwipeCoroutine(SwipeCard card, SwipeArea area, Vector2 initCardPosition)
	{
		StartCoroutine(SwipeCoroutine(card, area, initCardPosition));
	} 

	IEnumerator SwipeCoroutine(SwipeCard card, SwipeArea area, Vector2 initCardPosition)
	{
		var cardPos = card.transform.position;
		Vector2 swipeTarget = area.SwipeTarget.transform.position;

		Vector2 cardDir = (Vector2)cardPos - initCardPosition;

		Vector2 swipeEnd = Vector2.zero;
		if (area.Direction != SwipeDirection.None)
		{
			// get the local up or down direction it needs to hit
			Vector2 swipeNormal = cardDir;
			if (area.Direction == SwipeDirection.Up || area.Direction == SwipeDirection.Down)
				swipeNormal.y = 0;
			else
				swipeNormal.x = 0;
			swipeNormal.Normalize();
			
			// intersect point
			Vector3 intersectionPoint = Vector3.zero;
			bool foundIntersect = Math3d.LineLineIntersection(out intersectionPoint, cardPos, cardDir, swipeTarget, swipeNormal);

			swipeEnd = foundIntersect? (Vector2)intersectionPoint : swipeTarget;
		}
		else
			swipeEnd = swipeTarget;


		// find the procent it has already done of its path
		var swipeEndLength = (swipeEnd - initCardPosition).magnitude;
		var doneCardLength = cardDir.magnitude;
		

		// set loop values
		Vector2 from = Vector2.zero;
		Vector2 to = Vector2.zero;
		float f = 0;
		if (doneCardLength <= swipeEndLength)
		{
			f = doneCardLength / swipeEndLength;
			from = initCardPosition;
			to = swipeEnd;
		}
		else
		{
			to = swipeEnd;
			from = cardPos;
		}

		// move for the rest of the time
		for (; f <= 1.0f; f += Time.deltaTime * 6)
		{
			var pos = Vector2.Lerp(from, to, f);
			card.transform.position = pos;
			yield return null;
		}

		// trigger
		card.transform.position = to;
		OnCardSwiped.Invoke(area, card);
		area.OnCardSwiped.Invoke(area, card);

		// var c = ActiveCard;
		// ActiveCard = SecondCard;
		// c.transform.SetAsFirstSibling();
		// SecondCard = c;
	}
}
