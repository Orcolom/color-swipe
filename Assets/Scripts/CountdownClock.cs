﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class CountdownClock : MonoBehaviour
{
  public Slider Slider;
  public Text Label;
  public float CurrentTime;
  public float TimeRange = 1;
  public bool UseUpdate = true;
  public UnityEvent OnClockZero;

  void Awake()
  {
    SetTime(TimeRange);
  }

  void Update()
  {
    if (UseUpdate)
      AddTime(-Time.deltaTime);
  }

  public void SetTime(float f)
  {
    CurrentTime = f;
    CurrentTime = Mathf.Max(0, CurrentTime);

    if (f < 0)
      OnClockZero.Invoke();
    
    Slider.value = CurrentTime / TimeRange;
    Label.text = CurrentTime.ToString("0.0") + "s";
  }

  public void AddTime(float f)
  {
    CurrentTime += f;
    CurrentTime = Mathf.Max(0, CurrentTime);

    if (f < 0)
      OnClockZero.Invoke();
    
    Slider.value = CurrentTime / TimeRange;
    Label.text = CurrentTime.ToString("0.0") + "s";
  }
}
