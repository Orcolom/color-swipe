﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Globalization;

public class ColorCard : SwipeCard
{
	[Header("Looks")]

	public Text NameLabel;
	public Text DescriptionLabel;
	public Image Background;

	public void SetName(string name)
	{
		NameLabel.text = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(name.ToLower());
	}

	public void SetDesc(string desc)
	{
		DescriptionLabel.text = desc;
	}

	public void SetColor(Color c)
	{
		var value = (c.r + c.g + c.b) / 3;
		Background.color = c;
		NameLabel.color = DescriptionLabel.color = (value < .6f)? Color.white : Color.black;
	}
}
