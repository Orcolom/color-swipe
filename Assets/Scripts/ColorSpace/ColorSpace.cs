﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Text.RegularExpressions;
using UnityEngine.Networking;

public struct ColorPair
{
	public string Word;
	public Color Color;
}

public class ColorSpace
{
	public List<ColorData> RealColors = new List<ColorData>();
	public List<ColorData> FakeColors = new List<ColorData>();

	public List<ColorPair> PrimaryColorNames = new List<ColorPair>();
	public List<ColorPair> SupplementaryColorNames = new List<ColorPair>();

	public void RawToJSONFile(string rawFile, string jsonFile)
	{
		if (!File.Exists(rawFile))
			return;
		if (!File.Exists(jsonFile))
			return;

		string rawText = File.ReadAllText(rawFile);
		var words = Regex.Split(rawText, @"\s");
		
		ColorDataFile file = new ColorDataFile();
		List<ColorData> colors = new List<ColorData>();
		
		ColorData data = new ColorData();
		for(int i = 0; i < words.Length;)
		{
			var word = words[i];
			if (word.Length == 0)
			{
				i++;
				continue;
			}

			if (word[0] == '#')
			{
				data.Name = data.Name.Trim();
				data.Hex = word.Substring(1);
				
				colors.Add(data);
				data = new ColorData();

				i += 10;
			}
			else
			{
				data.Name += word + " ";
				i++;
			}
		}

		file.Colors = colors.ToArray();
		var text = JsonUtility.ToJson(file);
		File.WriteAllText(jsonFile, text);
	}

	public void AddPrimaryColorName(string name, string hex)
	{
		var word = name.ToLower();
		var idx = PrimaryColorNames.FindIndex((ColorPair p) => p.Word.ToLower() == word);

		var color = Color.black;
		ColorUtility.TryParseHtmlString("#" + hex, out color);
		color.a = 1;

		if (idx == -1)
			PrimaryColorNames.Add(new ColorPair() {Word = name, Color = color });
		else
		{
			var pair = PrimaryColorNames[idx];
			pair.Color += color;
			PrimaryColorNames[idx] = pair;
		}
	}

	public void AddSupplementaryColorName(string name, string hex)
	{
		var word = name.ToLower();
		var idx = SupplementaryColorNames.FindIndex((ColorPair p) => p.Word.ToLower() == word);

		var color = Color.black;
		ColorUtility.TryParseHtmlString("#" + hex, out color);
		color.a = 1;

		if (idx == -1)
			SupplementaryColorNames.Add(new ColorPair() {Word = name, Color = color });
		else
		{
			var pair = SupplementaryColorNames[idx];
			pair.Color += color;
			SupplementaryColorNames[idx] = pair;
		}
	}

	public string ColorsFromFile(string fullPath)
	{
		var str = "";
		
		string dataAsJSON = "";
		var platform = Application.platform;
		str += platform.ToString() + "\n";
		str += fullPath + "\n"; 
		if (platform == RuntimePlatform.Android)
		{
			WWW reader = new WWW(fullPath);
			while (!reader.isDone) {}
			str += reader.text;
			dataAsJSON = reader.text;
		} 
		else
			dataAsJSON = File.ReadAllText(fullPath);

		var file = JsonUtility.FromJson<ColorDataFile>(dataAsJSON);
		RealColors.AddRange(file.Colors);

		for (int i = 0; i < RealColors.Count; i++)
		{
			var color = RealColors[i];
			var idx = color.Name.IndexOf("(");
			
			if (idx != -1)
				color.Name = color.Name.Substring(0, idx);
			idx = color.Name.IndexOf("[");
			if (idx != -1)
				color.Name = color.Name.Substring(0, idx);
			
			RealColors[i] = color;
		}

		foreach (var color in RealColors)
		{
			var names = color.Name.Split(' ');
			if (names.Length == 1)
			{
				AddPrimaryColorName(names[0], color.Hex);
			}
			else
			{
				AddSupplementaryColorName(names[0], color.Hex);
				AddPrimaryColorName(names[1], color.Hex);
				for (int i = 2; i < names.Length; i++)
					AddSupplementaryColorName(names[i], color.Hex);
			}
		}

		for (int i = 0; i < RealColors.Count; i++)
		{
			var primary = PrimaryColorNames[Random.Range(0, PrimaryColorNames.Count)];
			var supplementary = SupplementaryColorNames[Random.Range(0, SupplementaryColorNames.Count)];

			ColorData color = new ColorData();
			color.IsFake = true;

			int y = Random.Range(0, 100);
			if (y < 16)
				color.Name = supplementary.Word + " " + primary.Word;
			else
				color.Name = primary.Word;

			int idx = RealColors.FindIndex( (ColorData c) => c.Name.ToLower() == color.Name.ToLower());
			if (idx >= 0)
				continue;

			idx = FakeColors.FindIndex( (ColorData c) => c.Name.ToLower() == color.Name.ToLower());
			if (idx >= 0)
				continue;

			var col = primary.Color + supplementary.Color;
			col.r /= col.a;
			col.g /= col.a;
			col.b /= col.a;
			col.a = 1;
			color.Hex = ColorUtility.ToHtmlStringRGB(col);
			
			

			FakeColors.Add(color);
		}

		return str;
	}


	public ColorData GetFakeRandomColor()
	{
		var f = Random.Range(0, FakeColors.Count);
		return FakeColors[f];
	}

	public ColorData GetRealRandomColor()
	{
		var f = Random.Range(0, RealColors.Count);
		return RealColors[f];
	}

	public ColorData GetRandomColor()
	{
		var realOrFake = Random.Range(0, 100);
		
		if (realOrFake > 40)
			return GetRealRandomColor();
		else
			return GetFakeRandomColor();
	}
}
